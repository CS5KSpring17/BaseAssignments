package assignment;

import java.text.MessageFormat;
import java.util.Scanner;
import java.io.PrintStream;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;

public class Assignment2 {
	
	/**
	 * This is a sample for you to get started.
	 * This method reads one integer from a Scanner it takes as parameter and returns twice the number read. 
	 * @return twice the number read, an int.
	 */
	public static int readAndDouble(Scanner in, PrintStream out)
	{
		out.println("Please enter a number");
		int number=in.nextInt();
		return 2*number;
	}

	
	/**
	 * YOU NEED TO IMPLEMENT THIS
	 * Need to read two numbers from the given Scanner (may want to print messages to the given PrintStream), and return their sum
	 * @return the sum of the two numbers read
	 */
	public static int readAndAdd(Scanner in, PrintStream out)
	{
		// TODO - you need to implement this
		return 0;
	}
	
	/** 
	 * YOU NEED TO IMPLEMENT THIS
	 * Need to read two numbers from the given Scanner (may want to print messages to the given PrintStream), and return their product
	 * @return the product of the two numbers read
	 */
	public static int readAndMultiply(Scanner in, PrintStream out)
	{
		// TODO - you need to implement this
		return 0;
	}

	/**
	 * You need to implement this
	 * This function takes two int parameters, and a PrintStream, and prints (to the given PrintStream) their sum, difference, product, quotient and remainder.
	 * Preferably, print a message before it, like "The sum of 3 and 5 is 8"
	 * @param number1
	 * @param number2
	 */
	// notice this is void, so you call it like: printAllOperatiions(1,2,System.out)
	public static void printAllOperations(int number1, int number2, PrintStream out)
	{
		// TODO - you need to implement this

	}

	//this is just sample code demonstrating aliasing
	public static void aliasing1()
	{
		Point pt1=new Point(10,10);
		Point pt2=new Point(5,5);

		Point alias=pt1;
		alias.x+=pt2.x;

		alias=pt2;
		alias.y+=5;

		int sumX=pt1.x+pt2.x;
		int sumY=pt1.y+pt2.y;

		System.out.println("sumX= "+sumX+ " -sumY= "+sumY);
	}

	// more sample code
	public static void playWithRectangles(){
		Rectangle r=new Rectangle(10, 10, 5, 7);
		System.out.println(r.x);
	}

	/*
	 * A point is to the left of another if its x coordinate is less than the other
	 */
	public static boolean isToTheLeft(Point p1, Point p2)
	{
		return p1.x < p2.x;
	}

	/*
	 * A point is to the right of another if its x coordinate is bigger than the other
	 */
	public static boolean isToTheRight(Point p1, Point p2)
	{
		// TODO - you need to implement this
		return true;
	}


	/* The rectangle has the top-left coordinates (in screen coordinates, y=0 is top, y grows down)
	 * So the bottom-right coordinate can be calculated by adding the width and height to x and y, respectively
	 */
	public Point getBottomRightCorner(Rectangle r)
	{
		return new Point(r.x+r.width, r.y+r.height);
	}

	/*
	 * Returns the center of the rectangle. The center is obtained by adding half the width and half the height to the x and y coordinates respectively
	 * Round DOWN (if needed) when calculating the center.
	 */
	public static Point getCenter(Rectangle r)
	{
		// TODO - you need to implement this
		return new Point(0,0);
	}


/**
 * TODO - you need to implement this function.
 * This function should:
 *   0. Create a Scanner from System.in
 *   1. Call readAndAdd to get a value (pass the Scanner and System.out)
 *   2. Call readAndMultiply to get another value (again, pass the Scanner and System.out)
 *   3. Call printAllOperations with those two values (in that order) and System.out
 * @param args unused, will see later
*/
	public static void main(String args[])
	{
		// TODO - you need to implement this
	}
	
}
