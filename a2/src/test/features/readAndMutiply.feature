Feature: readAndMultiply (25 pts)
  Scenario: 1 -- 2 4
    When Input is "2 4"
    Then readAndMultiply is 8

  Scenario: 2 -- 3 5
    When Input is "3 5"
    Then readAndMultiply is 15

  Scenario: 3 -- 8 22
    When Input is "8 22"
    Then readAndMultiply is 176


  Scenario: 4 -- -1 2
    When Input is "-1 2"
    Then readAndMultiply is -2

  Scenario: 5 -- -2 -3
    When Input is "-2 -3"
    Then readAndMultiply is 6


