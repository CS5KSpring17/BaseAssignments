Feature: main (25 pts)

  Scenario: 1 -- 2 4 -1 1
    When Input is "2 4 -1 1"
    Then main's output contains 5, 7, -6, -6 and 0

  Scenario: 2 -- 2 2 2 4
    When Input is "2 2 2 4"
    Then main's output contains 12, 4, 32, 2 and 0

  Scenario: 3 -- 2 1 5 2
    When Input is "2 1 5 2"
    Then main's output contains 13, -7, 30, 0 and 3

