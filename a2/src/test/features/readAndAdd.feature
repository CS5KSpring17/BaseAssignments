Feature: readAndAdd (25 pts)
  Scenario: 1 -- 2 4
    When Input is "2 4"
    Then readAndAdd is 6

  Scenario: 2 -- 3 5 => 8
    When Input is "3 5"
    Then readAndAdd is 8

  Scenario: 3 -- 8 22 => 30
    When Input is "8 22"
    Then readAndAdd is 30

  Scenario: 4 -- 8 -20 => -12
    When Input is "8 -20"
    Then readAndAdd is -12

  Scenario: 5 -- 0 0 => 0
    When Input is "0 0"
    Then readAndAdd is 0

  Scenario: 6 -- -1 -2 => -3
    When Input is "-1 -2"
    Then readAndAdd is -3

  Scenario: 7 -- 180 22 => 202
    When Input is "180 22"
    Then readAndAdd is 202
