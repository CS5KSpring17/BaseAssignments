Feature: get center (15 pts)
  Scenario: 1
  Given r1 is [1,3,0,0]
  Then r1's center is [1,3]


  Scenario: 2
    Given r1 is [1,3,4,4]
    Then r1's center is [3,5]


  Scenario: 3
    Given r1 is [1,3,5,5]
    Then r1's center is [3,5]

  Scenario: 4
    Given r1 is [10,30,40,40]
    Then r1's center is [30,50]




