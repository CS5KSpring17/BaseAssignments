package assignment;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.io.*;
import java.util.Scanner;
import java.awt.Point;
import java.awt.Rectangle;

import java.util.Map;
import java.util.HashMap;

import static assignment.Assignment2.*;

public class CucumberSteps {

    String input;


    PrintStream out=new PrintStream(new ByteArrayOutputStream());

    InputStream originalStdIn;
    PrintStream originalStdOut;

    int arg1, arg2;


    @Before
    public void captureIO()
    {
        originalStdIn=System.in;
        originalStdOut=System.out;
    }

    @After
    public void restoreIO()
    {
        System.setIn(originalStdIn);
        System.setOut(originalStdOut);
    }

    @When("^Input is \"([^\"]*)\"$")
    public void input_is(String input) throws Throwable {
        this.input=input;
    }

    @Then("^readAndAdd is (\\d+)$")
    public void readandadd_is(int target_ans) throws Throwable {
        Scanner in = new Scanner(new StringReader( input) );

        int ans=Assignment2.readAndAdd(in,out);
        Assert.assertEquals(target_ans, ans);
    }

    @Then("^readAndAdd is -(\\d+)$")
    public void readandadd_is_minus(int negAns) throws Throwable {
        Scanner in = new Scanner(new StringReader( input) );

        int ans=Assignment2.readAndAdd(in,out);
        Assert.assertEquals(-negAns, ans);
    }


    @Then("^readAndMultiply is (\\d+)$")
    public void readandMultiply_is(int target_ans) throws Throwable {
        Scanner in = new Scanner(new StringReader( input) );

        int ans=Assignment2.readAndMultiply(in,out);
        Assert.assertEquals(target_ans, ans);
    }

    @Then("^readAndMultiply is -(\\d+)$")
    public void readandMultiply_is_minus(int target_ans) throws Throwable {
        Scanner in = new Scanner(new StringReader( input) );

        int ans=Assignment2.readAndMultiply(in,out);
        Assert.assertEquals(-target_ans, ans);
    }

    @When("^printAllOperations is called with (\\d+) and (\\d+)$")
    public void printalloperations_is_called_with_and(int arg1, int arg2) throws Throwable {
        this.arg1=arg1;
        this.arg2=arg2;
    }

    @Then("^output contains (\\d+), -(\\d+), (\\d+), (\\d+) and (\\d+)$")
    public void output_contains_and(int arg1, int arg2, int arg3, int arg4, int arg5) throws Throwable {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        Assignment2.printAllOperations(this.arg1, this.arg2, ps);
        String output=os.toString("UTF-8");

        Assert.assertTrue(output+" Does not contain "+arg1,output.contains(Integer.toString(arg1)));
        Assert.assertTrue(output+" Does not contain "+arg2, output.contains(Integer.toString(arg2)));
        Assert.assertTrue(output+" Does not contain "+arg3,output.contains(Integer.toString(arg3)));
        Assert.assertTrue(output+" Does not contain "+arg4,output.contains(Integer.toString(arg4)));
        Assert.assertTrue(output+" Does not contain "+arg5,output.contains(Integer.toString(arg5)));
    }

    @Then("^main's output contains -?(\\d+), -?(\\d+), -?(\\d+), -?(\\d+) and -?(\\d+)$")
    public void main_s_output_contains_and(int arg1, int arg2, int arg3, int arg4, int arg5) throws Throwable {
        captureIO();
        System.setIn( new ByteArrayInputStream(input.getBytes("UTF-8")) );
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);

        Assignment2.main(null);

        String output=os.toString("UTF-8");
        Assert.assertTrue(output+" Does not contain "+arg1,output.contains(Integer.toString(arg1)));
        Assert.assertTrue(output+" Does not contain "+arg2, output.contains(Integer.toString(arg2)));
        Assert.assertTrue(output+" Does not contain "+arg3,output.contains(Integer.toString(arg3)));
        Assert.assertTrue(output+" Does not contain "+arg4,output.contains(Integer.toString(arg4)));
        Assert.assertTrue(output+" Does not contain "+arg5,output.contains(Integer.toString(arg5)));

        restoreIO();
    }


    Map<Integer,Point> points=new HashMap<>();
    Map<Integer,Rectangle> rectangles=new HashMap<>();

    static boolean is (String maybe) {
        return !maybe.contains("not");
    }

    @Given("^p(\\d+) is \\[(\\d+),(\\d+)\\]$")
    public void p_is(int place, int x, int y) throws Throwable {
        points.put(place, new Point(x,y));
    }

    @Then("^p(\\d+) (is|is not) to the right of p(\\d+)$")
    public void p_is_not_to_the_right_of_p(int arg1, String maybe, int arg2) throws Throwable {
        Assert.assertEquals(is(maybe), isToTheRight(points.get(arg1), points.get(arg2)));
    }


    @Given("^r(\\d+) is \\[(-?\\d+),(-?\\d+),(-?\\d+),(-?\\d+)\\]$")
    public void r_is(int recNum, int x, int y, int width, int height) throws Throwable {
        rectangles.put(recNum, new Rectangle(x,y,width,height));
    }

    @Then("^r(\\d+)'s center is \\[(-?\\d+),(-?\\d+)\\]$")
    public void r_s_center_is(int recNum, int x, int y) throws Throwable {
        Assert.assertEquals(new Point(x,y),getCenter(rectangles.get(recNum)));
    }

}
