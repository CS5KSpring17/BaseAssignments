package assignment;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.InputStream;
import java.io.PrintStream;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"html:build/reports/cucumber-html-report"},
        features ={ "src/test/features"}
)
public class CucumberTests {
    @BeforeClass
    public static void printGrades2() {
        System.out.println("\n\n\n  PRINTING GRADES\n\n\n");
    }


    @AfterClass
    public static void printGrades() {
        System.out.println("\n\n\n  PRINTING GRADES2\n\n\n");
        CucumberHooks.printGradeReport(System.out);
    }
}


