package assignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.okaram.grading.Grade;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;

import static assignment.Assignment10.*;

public class Grader {
	
	@Grade(points=25)
	@Test
	public void testReadAllDoubles1() throws Exception
	{		
		ArrayList<Double> arr=readAllDoublesFromFile("inputs/doubles1.txt");
		Assert.assertEquals(3, arr.size());
		Assert.assertEquals(1.2, arr.get(0), 0.1);
		Assert.assertEquals(3.4, arr.get(1), 0.1);
		Assert.assertEquals(5.6, arr.get(2), 0.1);
	}	

	@Grade(points=25)
	@Test
	public void testReadAllDoubles2() throws Exception
	{		
		ArrayList<Double> arr=readAllDoublesFromFile("inputs/doubles2.txt");
		Assert.assertEquals(3, arr.size());
		Assert.assertEquals(2.3, arr.get(0), 0.1);
		Assert.assertEquals(4.5, arr.get(1), 0.1);
		Assert.assertEquals(6.7, arr.get(2), 0.1);
	}	

	@Grade(points=25)
	@Test
	public void testSumIntsHappyPath() throws Exception
	{		
		int sum=intFileSum("inputs/ints1.txt");
		Assert.assertEquals(10, sum);

		sum=intFileSum("inputs/ints2.txt");
		Assert.assertEquals(56, sum);	
	}	

	@Grade(points=25)
	@Test
	public void testSumIntsExceptions() throws Exception
	{		
		int sum=intFileSum("inputs/noInts.txt");
		Assert.assertEquals(0, sum);
	}
}
