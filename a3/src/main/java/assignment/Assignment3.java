package assignment;

import java.text.MessageFormat;
import java.util.Scanner;

public class Assignment3 {


	/**
	 * use a switch statement for this function (no ifs allowed)
	 * 1=January, 2=February ... 12=December (notice they start with uppercase)
	 * return "Invalid Month" if not between 1 and 12.
	 * @return 
	 */

	public static String monthName(int month)
	{
		switch (month) {
			case 1:
				return "January";
			case 2:
				return "February";
			case 3:
				return "March";
			case 4:
				return "April";
			case 5:
				return "May";
			case 6:
				return "June";
			case 7:
				return "July";
			case 8:
				return "August";
			case 9:
				return "September";
			case 10:
				return "October";
			case 11:
				return "November";
			case 12:
				return "December";
			default:
				return "Invalid Month";
		}
	}

	/**
	 * use one or more if statements for this function (no switch allowed).
     * Hint: A chained if statement probably makes more sense 
	 * 1=January, 2=February ... 12=December (notice they start with uppercase)
	 * return "Invalid Month" if not between 1 and 12.
	 * @return 
	 */
	public static String monthName_if(int month)
	{
		return monthName(month);
    }
	
    /**
    * This function calculates the commision a company gives its vendor for a given product. The commission is calculated as follows (can safely assume amount is greater than 0):
    - if it is an annual license, the rates are:
        10% if sale is between $ 0 and $1,000
        15% if sale is between $1,001 and $10,000
        20% if sale is above $10,000
    - if it is a forever license (not an annual one; isAnnual will be false), the rates are:
        10% if sale is between $ 0 and $10,000
        15% if sale is between $10,001 and $50,000
        20% if sale is above $50,000

	 Notice your function should return the RATE as an integer, so the return value should be 10,15 or 20
    */   
    public static int commissionRate(boolean isAnnualLicense, int saleAmount)
    {
		if(isAnnualLicense) {
			if(saleAmount<=1000) {
				return 10;
			} else if (saleAmount<=10000) {
				return 15;
			} else {
				return 20;
			}
		} else {
			if(saleAmount<=10000) {
				return 10;
			} else if (saleAmount<=50000) {
				return 15;
			} else {
				return 20;
			}

		}
    }

    /* The AHA classifies diabetes (or normal sugar) depending on two different tests, according to the following table
      - if it is an FPG test
	  	+ <100 of blood glucose -> Normal 
    	+ 100-125  mg/dL of blood glucose -> Prediabetes
		+ 126 or more of blood glucose -> Diabetes
	 - if it is not an FPG test (it is an OGTT test)
	 	+ <140 of blood glucose -> Normal
		+ 140-199 of blood glucose -> Prediabetes
		+ 200+ of blood glucose -> Diabetes
	*/
    public static String diabetesStatus(boolean isFPGTest, int bloodGlucose)
    {
    	if(isFPGTest) {
			if (bloodGlucose<100)
				return "Normal";
			else if (bloodGlucose<=125)
				return "Prediabetes";
			else
				return "Diabetes";
		} else {
			if (bloodGlucose<140)
				return "Normal";
			else if (bloodGlucose<=199)
				return "Prediabetes";
			else
				return "Diabetes";
		}
    }
    

	public static void main(String[] args) {
		// Use as you see fit, not used for grading
		
		System.out.println(diabetesStatus(true, 120));
	}

}
