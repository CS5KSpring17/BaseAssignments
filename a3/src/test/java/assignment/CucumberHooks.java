package assignment;

import cucumber.api.Scenario;
import cucumber.api.java.After;

import java.io.File;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CucumberHooks {
    public static class FeatureData {
        public String featureName;
        public int points;
        public Set<String> failedScenarios=new TreeSet<>(), passedScenarios=new TreeSet<>();

        public FeatureData(String featureName, int points) {
            this.featureName = featureName;
            this.points = points;
        }

        public void addFailedScenario(String scenarioName) {
            failedScenarios.add(scenarioName);
        }
        public void addPassedScenario(String scenarioName) {
            passedScenarios.add(scenarioName);
        }

        public boolean hasRun() {return  !failedScenarios.isEmpty() || !passedScenarios.isEmpty();}
        public boolean hasPassed() {return failedScenarios.isEmpty() && !passedScenarios.isEmpty();}
        public boolean hasFailed() {return !failedScenarios.isEmpty(); }
    }

    private static class ScenarioData {
        public String featureName;
        public String scenarioName;
        public int points;
        static Pattern ptsPattern=Pattern.compile("\\(\\d+ pts\\)");

        static int getPoints(String featureName) {
//            System.out.println("\n\t\t\tGet Points "+featureName);
            Matcher m=ptsPattern.matcher(featureName);
            if(m.find()) {
//                System.out.println("matches");
                int start=m.start();
                int end=m.end();
                String ptsStr=featureName.substring(start+1, end-5);
//                System.out.println("\n\n found ptsStr"+ptsStr);
                return Integer.parseInt(ptsStr);
            } else {
//                System.out.println("Doesn't match");
            }
            return -1;
        }

        public static ScenarioData parseScenario(Scenario scenario) {
            String[] pieces=scenario.getId().split(";");

            ScenarioData sd=new ScenarioData();
            sd.featureName = pieces[0].replace('-',' ');
            sd.points=getPoints(sd.featureName);
            sd.scenarioName=scenario.getName();
            return sd;
        }


        @Override
        public String toString() {
            return "ScenarioData{" +
                    "featureName='" + featureName + '\'' +
                    ", scenarioName='" + scenarioName + '\'' +
                    ", points=" + points +
                    '}';
        }
    }

    public static Map<String, FeatureData> featureResults=new HashMap<>();

    public static FeatureData getFeatureData(String featureName, int points) {
        if(featureResults.containsKey(featureName)) {
            return featureResults.get(featureName);
        } else {
            FeatureData fd=new FeatureData(featureName,points);
            featureResults.put(featureName,fd);
            return fd;
        }
    }

    public static void addScenarioResult(Scenario scenario) {
        ScenarioData scenarioData=ScenarioData.parseScenario(scenario);
        FeatureData fd=getFeatureData(scenarioData.featureName, scenarioData.points);
        if(scenario.isFailed()) {
            fd.failedScenarios.add(scenarioData.scenarioName);
        } else {
            fd.passedScenarios.add(scenarioData.scenarioName);
        }
    }

    @After
    public static void after(Scenario scenario) {
        addScenarioResult(scenario);
    }

    public static void printGradeReport(PrintStream out) {
        int totalPoints=0, passedPoints=0;
        int totalFeatures=0, passedFeatures=0;
        out.println("\n\n------------------------------\n");
        for(FeatureData feature : featureResults.values()) {
            totalFeatures++;
            if(feature.points > 0) {
                totalPoints+=feature.points;
                if(feature.hasPassed()) {
                    passedPoints+=feature.points;
                }
            }

            if(feature.hasPassed()) {
                out.println("Feature " + feature.featureName + " Passed.");
                passedFeatures++;
            }
            if(feature.hasFailed()) {
                out.println("Feature " + feature.featureName + "Failed.");
                out.println("Failed Scenarios:");
                for (String scenario : feature.failedScenarios) {
                    out.println("\t"+scenario);
                }
            }
        }
        out.println("\n------------------------------");
        out.println("Passed: "+passedFeatures +" out of "+totalFeatures+" features");
        out.println("Grade: "+passedPoints +" out of "+totalPoints);
        out.println("------------------------------\n\n");


        out.println("For full report see file://"+ new File(".").getAbsolutePath()+ "/build/reports/cucumber-html-report/index.html");
    }
}
