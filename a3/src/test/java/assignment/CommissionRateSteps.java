package assignment;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class CommissionRateSteps {

    boolean isAnnualLicense;
    int saleAmount;

    @When("^licenseType is Annual$")
    public void licensetype_is_Annual() throws Throwable {
        isAnnualLicense=true;
    }

    @When("^licenseType is Forever$")
    public void licensetype_is_Forever() throws Throwable {
        isAnnualLicense=false;
    }

    @When("^saleAmount is (\\d+)$")
    public void saleamount_is(int saleAmount) throws Throwable {
        this.saleAmount=saleAmount;
    }

    @Then("^commisionRate is (\\d+)$")
    public void commisionrate_is(int targetRate) throws Throwable {
        Assert.assertEquals(targetRate, Assignment3.commissionRate(isAnnualLicense,saleAmount));
    }

}
