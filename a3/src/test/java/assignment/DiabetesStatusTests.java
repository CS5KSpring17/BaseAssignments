package assignment;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class DiabetesStatusTests {

    boolean isFPGTest;
    int bloodGlucose;

    @When("^testType is FPG$")
    public void testType_is_FPG() throws Throwable {
        isFPGTest=true;
    }

    @When("^testType is OGTT$")
    public void testType_is_OGTT() throws Throwable {
        isFPGTest=false;
    }

    @When("^blood glucose is (\\d+)$")
    public void blood_glucose_is(int bloodGlucose) throws Throwable {
        this.bloodGlucose=bloodGlucose;
    }

    @Then("^diabetes status is Normal$")
    public void diabetes_status_is_normal() throws Throwable {
        Assert.assertEquals("Normal", Assignment3.diabetesStatus(isFPGTest,bloodGlucose));
    }

    @Then("^diabetes status is Prediabetes$")
    public void diabetes_status_is_pre() throws Throwable {
        Assert.assertEquals("Prediabetes", Assignment3.diabetesStatus(isFPGTest,bloodGlucose));
    }
    @Then("^diabetes status is Diabetes$")
    public void diabetes_status_is_diabetes() throws Throwable {
        Assert.assertEquals("Diabetes", Assignment3.diabetesStatus(isFPGTest,bloodGlucose));
    }

}
