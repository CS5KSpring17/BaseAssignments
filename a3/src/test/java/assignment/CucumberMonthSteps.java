package assignment;


import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class CucumberMonthSteps {
    int monthNumber;

    @When("^monthNumber is (\\d+)$")
    public void monthnumber_is(int monthNumber) throws Throwable {
        this.monthNumber=monthNumber;
    }

    @Then("^monthName_if is \"([^\"]*)\"$")
    public void monthname_if_is(String monthName) throws Throwable {
        Assert.assertEquals(monthName, Assignment3.monthName_if(monthNumber));
    }

    @Then("^monthName is \"([^\"]*)\"$")
    public void monthname_is(String monthName) throws Throwable {
        Assert.assertEquals(monthName, Assignment3.monthName(monthNumber));
    }
}
