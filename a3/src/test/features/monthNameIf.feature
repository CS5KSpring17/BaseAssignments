Feature: MonthName_if (25 pts)
  Scenario: 1 -- January
    When monthNumber is 1
    Then monthName_if is "January"

  Scenario: 2 -- February
    When monthNumber is 2
    Then    monthName_if is "February"

  Scenario: 3 -- March
    When monthNumber is 3
    Then    monthName_if is "March"

  Scenario: 4 -- April
    When monthNumber is 4
    Then    monthName_if is "April"

  Scenario: 5 -- May
    When monthNumber is 5
    Then    monthName_if is "May"

  Scenario: 6 -- June
    When monthNumber is 6
    Then    monthName_if is "June"

  Scenario: 7 -- July
    When monthNumber is 7
    Then    monthName_if is "July"

  Scenario: 8 -- August
    When monthNumber is 8
    Then    monthName_if is "August"

  Scenario: 9 -- September
    When monthNumber is 9
    Then    monthName_if is "September"

  Scenario: 10 -- October
    When monthNumber is 10
    Then    monthName_if is "October"

  Scenario: 11 -- November
    When monthNumber is 11
    Then    monthName_if is "November"

  Scenario: 12 -- December
    When monthNumber is 12
    Then    monthName_if is "December"

