Feature: MonthName (25 pts)
  Scenario: 1 -- January
    When monthNumber is 1
    Then    monthName is "January"

  Scenario: 2 -- February
    When monthNumber is 2
    Then    monthName is "February"

  Scenario: 3 -- March
    When monthNumber is 3
    Then    monthName is "March"

  Scenario: 4 -- April
    When monthNumber is 4
    Then    monthName is "April"

  Scenario: 5 -- May
    When monthNumber is 5
    Then    monthName is "May"

  Scenario: 6 -- June
    When monthNumber is 6
    Then    monthName is "June"

  Scenario: 7 -- July
    When monthNumber is 7
    Then    monthName is "July"

  Scenario: 8 -- August
    When monthNumber is 8
    Then    monthName is "August"

  Scenario: 9 -- September
    When monthNumber is 9
    Then    monthName is "September"

  Scenario: 10 -- October
    When monthNumber is 10
    Then    monthName is "October"

  Scenario: 11 -- November
    When monthNumber is 11
    Then    monthName is "November"

  Scenario: 12 -- December
    When monthNumber is 12
    Then    monthName is "December"

