Feature: diabetesStatus (25 pts)

  Scenario: FPG, 50
    When testType is FPG
    And blood glucose is 50
    Then diabetes status is Normal

  Scenario: FPG, 99
    When testType is FPG
    And blood glucose is 99
    Then diabetes status is Normal

  Scenario: FPG, 100
    When testType is FPG
    And blood glucose is 100
    Then diabetes status is Prediabetes

  Scenario: FPG, 115
    When testType is FPG
    And blood glucose is 115
    Then diabetes status is Prediabetes

  Scenario: FPG, 125
    When testType is FPG
    And blood glucose is 125
    Then diabetes status is Prediabetes

  Scenario: FPG, 126
    When testType is FPG
    And blood glucose is 126
    Then diabetes status is Diabetes

  Scenario: FPG, 150
    When testType is FPG
    And blood glucose is 150
    Then diabetes status is Diabetes


  Scenario: OGTT, 50
    When testType is OGTT
    And blood glucose is 50
    Then diabetes status is Normal

  Scenario: OGTT, 139
    When testType is OGTT
    And blood glucose is 139
    Then diabetes status is Normal

  Scenario: OGTT, 140
    When testType is OGTT
    And blood glucose is 140
    Then diabetes status is Prediabetes

  Scenario: OGTT, 150
    When testType is OGTT
    And blood glucose is 150
    Then diabetes status is Prediabetes

  Scenario: OGTT, 199
    When testType is OGTT
    And blood glucose is 199
    Then diabetes status is Prediabetes

  Scenario: OGTT, 200
    When testType is OGTT
    And blood glucose is 200
    Then diabetes status is Diabetes

  Scenario: OGTT, 250
    When testType is OGTT
    And blood glucose is 250
    Then diabetes status is Diabetes
