Feature: CommissionRate (25 pts)

  Scenario: Annual, 0
    When licenseType is Annual
    And saleAmount is 0
    Then commisionRate is 10

  Scenario: Annual, 500
    When licenseType is Annual
    And saleAmount is 500
    Then commisionRate is 10

  Scenario: Annual, 1000
    When licenseType is Annual
    And saleAmount is 1000
    Then commisionRate is 10

  Scenario: Annual, 1001
    When licenseType is Annual
    And saleAmount is 1001
    Then commisionRate is 15

  Scenario: Annual, 6000
    When licenseType is Annual
    And saleAmount is 6000
    Then commisionRate is 15

  Scenario: Annual, 10000
    When licenseType is Annual
    And saleAmount is 10000
    Then commisionRate is 15

  Scenario: Annual, 10001
    When licenseType is Annual
    And saleAmount is 10001
    Then commisionRate is 20

  Scenario: Annual, 50000
    When licenseType is Annual
    And saleAmount is 50000
    Then commisionRate is 20

  Scenario: Forever, 0
    When licenseType is Forever
    And saleAmount is 0
    Then commisionRate is 10

  Scenario: Forever, 500
    When licenseType is Forever
    And saleAmount is 500
    Then commisionRate is 10

  Scenario: Forever, 10000
    When licenseType is Forever
    And saleAmount is 10000
    Then commisionRate is 10

  Scenario: Forever, 10001
    When licenseType is Forever
    And saleAmount is 10001
    Then commisionRate is 15

  Scenario: Forever, 20000
    When licenseType is Forever
    And saleAmount is 20000
    Then commisionRate is 15

  Scenario: Forever, 50000
    When licenseType is Forever
    And saleAmount is 50000
    Then commisionRate is 15

  Scenario: Forever, 50001
    When licenseType is Forever
    And saleAmount is 50001
    Then commisionRate is 20

  Scenario: Forever, 120000
    When licenseType is Forever
    And saleAmount is 120000
    Then commisionRate is 20

