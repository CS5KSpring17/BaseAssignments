package assignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.junit.*;

import com.okaram.grading.Grade;


public class Grader {

    @Ignore
	@Grade(points=25)
	@Test
	public void testStringTimes1()
	{
		Assert.assertEquals("", Assignment4.stringTimes("abc", 0));
		Assert.assertEquals("abc", Assignment4.stringTimes("abc", 1));

		Assert.assertEquals("HiHiHiHi", Assignment4.stringTimes("Hi", 4));
		Assert.assertEquals("123123123", Assignment4.stringTimes("123", 3));
	}



	@Ignore
	@Grade(points=25)
	@Test
	public void testReadWithinRange()
	{
        InputStream in = new ByteArrayInputStream( " 10 20".getBytes() );
        PrintStream out=new PrintStream(new ByteArrayOutputStream());
        
        Scanner in_s=new Scanner(in);
        int ans=Assignment4.readWithinRange(in_s,out,15,25);
        Assert.assertEquals(20, ans);
        
        in_s=new Scanner(new ByteArrayInputStream( "10 20".getBytes() ));
        ans=Assignment4.readWithinRange(in_s,out,10,20);
        Assert.assertEquals(10, ans);
        in_s=new Scanner(new ByteArrayInputStream( "10 20 21".getBytes() ));
        Assert.assertEquals(21, Assignment4.readWithinRange(in_s,out,21,30));        
   	}

    @Ignore
    @Grade(points=25)
    @Test
    public void testIsPrime()
    {
        Assert.assertEquals("1 is not prime", false,Assignment4.isPrime(1));
        Assert.assertEquals("2 is prime", true,Assignment4.isPrime(2));
        Assert.assertEquals("4 is not prime", false,Assignment4.isPrime(4));
        Assert.assertEquals("7 is prime", true,Assignment4.isPrime(7));
        Assert.assertEquals("9 is not prime", false,Assignment4.isPrime(9));
        Assert.assertEquals("35 is not prime", false,Assignment4.isPrime(35));
        Assert.assertEquals("37 is prime", true,Assignment4.isPrime(37));        
	
		Assert.assertEquals("49 is not prime", false, Assignment4.isPrime(49));
		Assert.assertEquals("169 is not prime", false, Assignment4.isPrime(169));
		Assert.assertEquals("53 is prime",true,Assignment4.isPrime(53));
	}
        private static String getPFBOutput(int from, int to)
        {
    		ByteArrayOutputStream out1=new ByteArrayOutputStream();
	    	PrintStream out=new PrintStream(out1);
		
            Assignment4.printFizzBuzz(from,to,out);
    		String output=new String(out1.toByteArray());
            return output;            
        }

    @Ignore
    @Grade(points=25)
    @Test
    public void testPrintFizzBuzz()
    {
        String ans1="1\n2\n".replaceAll("\n",System.lineSeparator());
        String ans2="Fizz\n4\nBuzz\n".replaceAll("\n",System.lineSeparator()) ;
        String ans3="Fizz\n13\n14\nFizzBuzz\n16\n".replaceAll("\n",System.lineSeparator());
        Assert.assertEquals("printFizzBuzz(1,2) should print "+ans1,ans1,getPFBOutput(1,2));
        Assert.assertEquals("printFizzBuzz(3,5) should print "+ans2,ans2 ,getPFBOutput(3,5));
        Assert.assertEquals("printFizzBuzz(12,16) should print "+ans3,ans3 ,getPFBOutput(12,16));
       
    }
    
}
