package assignment;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"html:build/reports/cucumber-html-report"},
        features ={ "src/test/features"}
)
public class CucumberTests {

    @AfterClass
    public static void printGrades() {

//        okaram.cucumberTesting.CucumberHooks.printGradeReport(System.out);
        CucumberHooks.printGradeReport(System.out);
    }
}


