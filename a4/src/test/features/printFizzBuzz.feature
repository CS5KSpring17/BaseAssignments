Feature: printFizzBuzz (25 pts)
  Scenario: printFizzBuzz(1,3)
  Then printFizzBuzz of 1 and 3 should print '1,2,Fizz'


  Scenario: printFizzBuzz(14,19)
    Then printFizzBuzz of 14 and 19 should print '14,FizzBuzz,16,17,Fizz,19'

  Scenario: printFizzBuzz(4,7)
    Then printFizzBuzz of 4 and 7 should print '4,Buzz,Fizz,7'

