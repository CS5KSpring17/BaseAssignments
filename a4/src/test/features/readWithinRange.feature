Feature: readWithinRange (25 pts)
  Scenario: 1 - range 25,35 ; 10 20 30
  When input is "10 20 30"
  And range is 25,35
  Then readWithinRange is 30

  Scenario: 2 - range -10,10 ; -12 -8 12 8
    When input is "-12 -8 12 8"
    And range is -10,10
    Then readWithinRange is -8

  Scenario: 3 -
    When input is "10 20 30"
    And range is 10,21
    Then readWithinRange is 10

  Scenario: 4 -
    When input is "10 20 30 18"
    And range is 11,19
    Then readWithinRange is 18

