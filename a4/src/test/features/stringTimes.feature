Feature: stringTimes (25 pts)
  Scenario: 1 - "abc" times 4
  When the string is "abc"
  And the number of times is 4
  Then stringTimes is "abcabcabcabc"

  Scenario: 2 -- empty string
    When the string is ""
    And the number of times is 3
    Then stringTimes is ""

  Scenario: 3 -- times 0
    When the string is "a"
    And the number of times is 0
    Then stringTimes is ""

  Scenario: 4 -- "12" times 5
    When the string is "12"
    And the number of times is 5
    Then stringTimes is "1212121212"
