Feature: person formal full name (15 pts)
  Scenario: 1 - Orlando Karam
    Given person1 is ["Orlando", "Karam", 20]
    Then person1's formal full name is "Karam, Orlando"

  Scenario: 2 - Michelle karam
    Given person1 is ["Michelle", "Karam", 21]
    Then person1's formal full name is "Karam, Michelle"

  Scenario: 1 - Lina Colli
    Given person1 is ["Lina", "Colli", 22]
    Then person1's formal full name is "Colli, Lina"
