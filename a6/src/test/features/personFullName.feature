Feature: person full name (15 pts)
  Scenario: 1 - Orlando Karam
    Given person1 is ["Orlando", "Karam", 20]
    Then person1's full name is "Orlando Karam"

  Scenario: 2 - Michelle karam
    Given person1 is ["Michelle", "Karam", 21]
    Then person1's full name is "Michelle Karam"

  Scenario: 1 - Lina Colli
    Given person1 is ["Lina", "Colli", 22]
    Then person1's full name is "Lina Colli"
