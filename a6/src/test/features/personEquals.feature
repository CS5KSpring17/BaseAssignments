Feature: person equality (15 pts)
  Scenario: 1 - person equal to itself
    Given person1 is ["Orlando", "Karam", 20]
    Then person1 is equal to person1

  Scenario: 2 - people equal all fields are equal
    Given person1 is ["Lina", "Colli", 20]
      And person2 is ["Lina", "Colli", 20]
    Then person1 is equal to person2

  Scenario: 2a - people equal all fields are equal
    Given person1 is ["Orlando", "Karam", 20]
      And person2 is ["Orlando", "Karam", 20]
    Then person1 is equal to person2

  Scenario: 3 - different age are not equal
    Given person1 is ["Orlando", "Karam", 20]
      And person2 is ["Orlando", "Karam", 21]
    Then person1 is not equal to person2

  Scenario: 4 - different last name not equal
    Given person1 is ["Orlando", "Karamo", 21]
      And person2 is ["Orlando", "Karam", 21]
    Then person1 is not equal to person2

  Scenario: 5 - different first name not equal
    Given person1 is ["Michelle", "Karam", 20]
      And person2 is ["Orlando", "Karam", 20]
    Then person1 is not equal to person2




