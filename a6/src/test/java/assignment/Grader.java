package assignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.okaram.grading.Grade;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;

import static assignment.Assignment6.*;

public class Grader {

	@Grade(points=10)
	@Test
	public void testTurtleConstructor()
	{
		Turtle t=new Turtle();
		Assert.assertEquals(10,  t.getX());
		Assert.assertEquals(10,  t.getY());
	}
	
	@Grade(points=10)
	@Test
	public void testMoveNorth()
	{
		Turtle t=new Turtle();
		t.moveNorth();
		t.moveNorth();
		t.moveNorth();
		
		Assert.assertEquals(13,  t.getY());
	}

	@Grade(points=10)
	@Test
	public void testMoveSouth()
	{
		Turtle t=new Turtle();
		t.moveSouth();
		
		Assert.assertEquals(9,  t.getY());
	}
	
	@Grade(points=10)
	@Test
	public void testMoveEast()
	{
		Turtle t=new Turtle();
		t.moveEast();
		t.moveEast();
		
		Assert.assertEquals(12,  t.getX());
	}
	
	@Grade(points=10)
	@Test
	public void testMoveWest()
	{
		Turtle t=new Turtle();
		t.moveWest();
		t.moveWest();
		t.moveWest();
		t.moveWest();
		
		Assert.assertEquals(6,  t.getX());
	}

	@Grade(points=10)
	@Test
	public void testMoveInDirection()
	{
		Turtle t=new Turtle();
		t.move("East");
		t.move("South");
		
		Assert.assertEquals(11,  t.getX());
		Assert.assertEquals(9,  t.getY());
		t.move("West");
		t.move("North");
		Assert.assertEquals(10,  t.getX());
		Assert.assertEquals(10,  t.getY());
	}

	@Grade(points=0)
	@Test
	public void testMoveTurtleEast()
	{
		Turtle t=new Turtle();
		moveTurtleEast(t, 10);		
		Assert.assertEquals(20,  t.getX());
	}

	@Grade(points=10)
	@Test
	public void testMoveTurtleNorth()
	{
		Turtle t=new Turtle();
		moveTurtleNorth(t, 10);		
		Assert.assertEquals(20,  t.getY());
	}

	@Grade(points=0)
	@Test
	public void testMoveTurtleSouth()
	{
		Turtle t=new Turtle();
		moveTurtleSouth(t, 10);		
		Assert.assertEquals(0,  t.getY());
	}

	@Grade(points=10)
	@Test
	public void testMoveTurtleWest()
	{
		Turtle t=new Turtle();
		moveTurtleWest(t, 8);		
		Assert.assertEquals(2,  t.getX());
	}
	
	@Grade(points=10)
	@Test
	public void testEquals()
	{
		Turtle t1=new Turtle();
		Turtle t2=new Turtle();
		
		Assert.assertTrue(t1.equals(t2));
	}
	
	@Grade(points=10)
	@Test
	public void testMoveTurtleTo()
	{
		Turtle t=new Turtle();
		moveTurtleTo(t, 15,16);
		Assert.assertEquals(15,  t.getX());
		Assert.assertEquals(16,  t.getY());
		
		moveTurtleTo(t, 0,0);		
		Assert.assertEquals(0,  t.getX());
		Assert.assertEquals(0,  t.getY());
	}
}
