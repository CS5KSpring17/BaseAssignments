package assignment;


/**
 * This class represents a turtle, like a Logo-like turtle, which keeps its position.
 * The turtle keeps its x and y position, and has methods moveNorth() etc which change it
 * Assume normal math coordinates and map convention, so as you move up (North) y increases, and as you move East x increases.
 *
 */
public class Turtle {

	// HINT - you may want to have variables to keep the position. Keep these variables private,
	
	
	// TODO - The empty constructor initializes position to 10,10
	public Turtle() {
	}
		
	public int getX() { 
		// TODO - implement this
		return 0;
	}
	public int getY() {
		// TODO - implement this
		return 0;
	}
	
	public void moveNorth() {
		// TODO - implement this. this increments the y coordinate by one unit
	}
	
	public void moveSouth() {
		// TODO - implement this. this decrements the y coordinate by one unit
	}
	
	public void moveEast() {
		// TODO - this increments the x coordinate by one unit
	}
	
	public void moveWest() {
		// TODO - this decrements the x coordinate by one unit
	}
	
	public String toString() {
		return "Turtle[x="+getX()+", y="+getY()+"]";
	}
	
	public boolean equals(Turtle t)
	{
		// TODO - you need to implement this
		// two turtles are equal if their X and Y coordinates are equal.
		return false;
	}
	
	public void move(String direction)
	{
		// TODO - you need to implement this
		// move to the right direction; direction can be North, South, East, West
	}
}
