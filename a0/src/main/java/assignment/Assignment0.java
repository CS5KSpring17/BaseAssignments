package assignment;

public class Assignment0 {
	
	public static int add(int a, int b){
		// TODO - replace line below with return a+b;  to pass tests 
		return 0;
	}
	
	public static int subtract(int a, int b){
		// TODO - replace line below with return a-b;  to pass tests 
		return 0;
	}


	public static void main(String args[]){
		// this is just sample code, feel free to modify and call the functions above
		// with different parameters
		System.out.println("20 + 10 = "+add(20,10));
		System.out.println("20 - 10 = "+subtract(20,10));
	}
}
