Feature: subtract (50 pts)

  Scenario: 1 -- subtract(1,2) returns -1
    When a is 1
    And  b is 2
    Then subtract(a,b) returns -1

  Scenario: 2 -- subtract(10,2) returns 8
    When a is 10
    And  b is 2
    Then subtract(a,b) returns 8

  Scenario: 3 -- subtract(21,2) returns 19
    When a is 21
    And  b is 2
    Then subtract(a,b) returns 19

  Scenario: 4 -- subtract(12,20) returns -8
    When a is 12
    And  b is 20
    Then subtract(a,b) returns -8
