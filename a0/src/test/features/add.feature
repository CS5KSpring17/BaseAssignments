Feature: add (50 pts)

  Scenario: 1 -- add(1,2) returns 3
    When a is 1
    And  b is 2
    Then add(a,b) returns 3

  Scenario: 2 -- add(-1,-2) returns -3
    When a is -1
    And  b is -2
    Then add(a,b) returns -3

  Scenario: 3 -- add(10,123) returns 133
    When a is 10
    And  b is 123
    Then add(a,b) returns 133

  Scenario: 4 -- add(-1,2) returns 1
    When a is -1
    And  b is 2
    Then add(a,b) returns 1

  Scenario: 5 -- add(1,-2) returns -1
    When a is 1
    And  b is -2
    Then add(a,b) returns -1

  Scenario: 6 -- add(0,0) returns 0
    When a is 0
    And  b is 0
    Then add(a,b) returns 0

