package assignment;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.io.*;
import java.util.Scanner;

public class CucumberSteps {

    int a,b;

    @When("^a is (-?\\d+)$")
    public void a_is(int a) throws Throwable {
        this.a=a;
    }

    @When("^b is (-?\\d+)$")
    public void b_is(int b) throws Throwable {
        this.b=b;
    }

    @Then("^add\\(a,b\\) returns (-?\\d+)$")
    public void add_a_b_returns(int arg1) throws Throwable {
        Assert.assertEquals(arg1, Assignment0.add(a,b));
    }

    @Then("^subtract\\(a,b\\) returns (-?\\d+)$")
    public void subtract_a_b_returns(int arg1) throws Throwable {
        Assert.assertEquals(arg1, Assignment0.subtract(a,b));
    }


}
