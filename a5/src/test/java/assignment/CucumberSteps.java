package assignment;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.io.*;
import java.util.Scanner;

import static assignment.Assignment5.*;

public class CucumberSteps {

    String theString, input;
    int times, low,high;

    PrintStream out=new PrintStream(new ByteArrayOutputStream());

    InputStream originalStdIn;
    PrintStream originalStdOut;


    static boolean is(String maybe) {
        return !maybe.contains("not");
    }


    @When("^the string is \"([^\"]*)\"$")
    public void the_string_is(String s) throws Throwable {
        this.theString=s;
    }

    @When("^the number of times is (\\d+)$")
    public void the_number_of_times_is(int n) throws Throwable {
        this.times=n;
    }

    @Then("^stringTimes is \"([^\"]*)\"$")
    public void stringtimes_is(String arg1) throws Throwable {
        Assert.assertEquals(arg1, stringTimes(theString,times));
    }

    @When("^input is \"([^\"]*)\"$")
    public void input_is(String input) throws Throwable {
        this.input=input;
    }

    @When("^range is (-?\\d+),(-?\\d+)$")
    public void range_is(int low, int high) throws Throwable {
        this.low=low;
        this.high=high;
    }

    @Then("^readWithinRange is (-?\\d+)$")
    public void readwithinrange_is(int target) throws Throwable {
        InputStream in = new ByteArrayInputStream( input.getBytes() );
        Scanner in_s=new Scanner(in);
        int ans=readWithinRange(in_s,System.out,low,high);
        Assert.assertEquals(target, ans);
    }

    @Then("^(\\d+) (is|is not) prime$")
    public void is_prime(int arg1, String maybe) throws Throwable {
        Assert.assertEquals(is(maybe), isPrime(arg1));
    }

    @Then("^printFizzBuzz of (\\d+) and (\\d+) should print '(.*)'$")
    public void prinfFB_should_print(int from, int to, String word_string) throws Throwable {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        printFizzBuzz(from,to,ps);

        InputStream istream = new ByteArrayInputStream( os.toString().getBytes() );
        Scanner in=new Scanner(istream);
        String[] words= word_string.split(",");
        for(String word : words) {
            String w2=in.next();
            Assert.assertEquals(word,w2);
        }
    }

    @Then("^readAndSumPositives is (\\d+)$")
    public void readandsumpositives_is(int target) throws Throwable {
        InputStream in = new ByteArrayInputStream( input.getBytes() );
        Scanner in_s=new Scanner(in);
        int ans=readAndSumPositives(in_s, System.out);
        Assert.assertEquals(target, ans);
    }

    @Then("^productFromTo\\((-?\\d+),(-?\\d+)\\) is (-?\\d+)$")
    public void product_from_to(int from, int to, int ans) throws Throwable {
        Assert.assertEquals(ans,productFromTo(from,to));
    }

}
