Feature: productFromTo (15 pts)
  Scenario: 1 - 1,5
  Then productFromTo(1,5) is 120

  Scenario: 2 - -1,5
    Then productFromTo(-1,5) is 0

  Scenario: 3 - -3,-1
    Then productFromTo(-3,-1) is -6

  Scenario: 4 - 5,10
    Then productFromTo(5,10) is 0
