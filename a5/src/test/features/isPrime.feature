Feature: isPrime (20 pts)
  Scenario: 1 - 1 is not prime
  Then 1 is not prime

  Scenario: 2 - 2 is prime
    Then 2 is prime

  Scenario: 3 - 3 is prime
    Then 3 is prime

  Scenario: 4 - 4 is not prime
    Then 4 is not prime

  Scenario: 5 - 7 is prime
    Then 7 is prime

  Scenario: 6 - 120 is not prime
    Then 120 is not prime

  Scenario: 7 - 49 is not prime
    Then 49 is not prime

  Scenario: 8 - 169 is not prime
    Then 169 is not prime

  Scenario: 9 - 171 is not prime
    Then 171 is not prime

  Scenario: 10 - 1013 is prime
    Then 1013 is prime

  Scenario: 11 - 1015 is not prime
    Then 1015 is not prime
