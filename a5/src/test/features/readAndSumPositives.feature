Feature: readAndSumPositives (20 pts)
  Scenario: 1 - 10 20 30
  When input is "10 20 30"
  Then readAndSumPositives is 60

  Scenario: 2 - 10 -20 30
    When input is "10 -20 30"
    Then readAndSumPositives is 40

  Scenario: 3 - 1 -2 -3 4 5
    When input is "1 -2 -3 4 5"
    Then readAndSumPositives is 10

  Scenario: 4 - -1 -2 -3
    When input is "-1-2 -3"
    Then readAndSumPositives is 0




