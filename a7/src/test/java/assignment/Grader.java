package assignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.okaram.grading.Grade;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;

import static assignment.Assignment7.*;

public class Grader {

	@Grade(points=10)
	@Test
	public void testGetNumberOfReviews()
	{		
		GameInfo gi=new GameInfo("SomeGame");
		Assert.assertEquals(0,gi.getNumberOfReviews());
		gi.addReview(new Review("cool",5));
		Assert.assertEquals(1,gi.getNumberOfReviews());

		gi.addReview(new Review("terrible",1));
		Assert.assertEquals(2,gi.getNumberOfReviews());
	}
	
	@Grade(points=15)
	@Test
	public void testGetSumOfStars()
	{		
		GameInfo gi=new GameInfo("SomeGame");
		Assert.assertEquals(0,gi.getSumOfStars());
		gi.addReview(new Review("cool",5));
		Assert.assertEquals(5,gi.getSumOfStars());

		gi.addReview(new Review("cool",5));
		Assert.assertEquals(10,gi.getSumOfStars());
		gi.addReview(new Review("ok",3));
		Assert.assertEquals(13,gi.getSumOfStars());
		gi.addReview(new Review("terrible",1));
		Assert.assertEquals(14,gi.getSumOfStars());
	}
	
	@Grade(points=15)
	@Test
	public void testAverageStarRating()
	{		
		GameInfo gi=new GameInfo("SomeGame");
		gi.addReview(new Review("cool",5));
		Assert.assertEquals(5,gi.getAverageStarRating(),0.1);

		gi.addReview(new Review("cool",5));
		Assert.assertEquals(5,gi.getAverageStarRating(),0.1);
		gi.addReview(new Review("ok",3));
		Assert.assertEquals(4.33,gi.getAverageStarRating(),0.1);
		gi.addReview(new Review("terrible",1));
		Assert.assertEquals(3.5,gi.getAverageStarRating(),0.1);
	}
	
	@Grade(points=15)
	@Test
	public void testGetArrayProduct()
	{		
		int[] arr1={1,2,3,4,5};
		Assert.assertEquals(120,getArrayProduct(arr1));

		int[] arr2={1,2,0,4,5};
		Assert.assertEquals(0,getArrayProduct(arr2));

		int[] arr3={2,2,2,2,2};
		Assert.assertEquals(32,getArrayProduct(arr3));
	}
	
	@Grade(points=15)
	@Test
	public void testMax()
	{		
		int[] arr1={1,2,3,4,5};
		Assert.assertEquals(5,max(arr1));

		int[] arr2={1,2,0,40,5};
		Assert.assertEquals(40,max(arr2));

		int[] arr3={2,2,2,2,2};
		Assert.assertEquals(2,max(arr3));
	}

	@Grade(points=15)
	@Test
	public void testGetLowestX()
	{	
		Point[] arr1={new Point(1,3), new Point(2,2), new Point(3,1)};
		Assert.assertEquals(1,getLowestX(arr1));

	}
	
	@Grade(points=15)
	@Test
	public void testGetTopLeftCorner()
	{	
		Point[] arr1={new Point(1,3), new Point(2,2), new Point(3,1)};
		Assert.assertEquals(new Point(1,1),getTopLeftCorner(arr1));
	}
	
	
}
