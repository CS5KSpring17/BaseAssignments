package assignment;

import java.text.MessageFormat;
import java.util.Scanner;

import java.awt.Point;
import java.awt.Dimension;
import java.awt.Rectangle;

import java.time.LocalDate;

// TODO - Modify GameInfo.java to pass the tests
// Notice GameInfo uses Review type; see Review.java

public class Assignment7 {
	
	// TODO - calculate the product of an array of ints. Can assume array is not empty
	public static int getArrayProduct(int[] arr) {
		return 0;
	}
	
	// TODO - this function returns the largest element in an array. Can assume array is not empty
	public static int max(int[] arr) {
		// we assume the array is not empty
		return 0;
	}

	// TODO - get the x coordinate of the point with the lowest X coordinate in the array
	// can assume array is not empty
	public static int getLowestX(Point[] arr)
	{
		return 0;
	}
	
	// TODO - get a point representing the top-left corner of a given array of points
	// this is a point with the lowest X and the lowest Y (notice this may not be a point in the array)
	// so, for points (1,5), (2,4), (3,3) the top-left corner would be (1,3), since 1 is the lowest X and 3 is the lowest Y
	// can assume array is not empty
	public static Point getTopLeftCorner(Point[] arr) {
		return null;
	}
	
	
	public static void main(String[] args) {
		// you can use this as you wish to test or exercise your function. Not graded.
	}
}


