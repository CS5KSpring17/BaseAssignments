package assignment;

// This class keeps track of a 'bunch' of reviews, for a given game.
// We build it with the title of the game, and then we can call addReview to add 
class GameInfo {
	private String title;
	// need an array to keep the reviews; I suggest as below, but feel free to use something else
	// your array needs to be able to contain *at least 10* reviews. My tests will not add more than 10 (later we'll see better collections)
	// private Review[] reviews;
	// can add any other fields; for example, one to keep track of how many reviews you actually have (your array may contain up to 10, but at any given time may have way less; even 0)
	public GameInfo(String title) {
		this.title=title;
		// you may want to initialize any other variables you create here
	}
	
	public String getTitle() {
		return title;
	}
	
	
	// TODO - adds the review to the 'array' of reviews. You need to keep all reviews in an array
	public void addReview(Review r) {
	}

	// TODO - returns the number of reviews which have been added to this GameInfo
	public int getNumberOfReviews() {
		return 0;
	}
	
	// TODO - returns the sum of the number of stars which have been added to this GameInfo
	// you have to calculate this from your array
	public int getSumOfStars() {
		return 0;
	}

	// TODO - returns the average number of stars for this GameInfo's reviews
	// again, have to calculate this (or at least the sum of stars) from your array
	public double getAverageStarRating() {
		return 0;
	}
}
