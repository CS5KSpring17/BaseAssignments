package assignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.okaram.grading.Grade;
import static assignment.Assignment9.*;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;


public class Grader {

	@Grade(points=15)
	@Test
	public void testGetNumberOfReviews()
	{		
		GameInfo gi=new GameInfo("SomeGame");
		Assert.assertEquals(0,gi.getNumberOfReviews());
		gi.addReview(new Review("cool",5));
		Assert.assertEquals(1,gi.getNumberOfReviews());

		gi.addReview(new Review("terrible",1));
		Assert.assertEquals(2,gi.getNumberOfReviews());
	}
	
	@Grade(points=15)
	@Test
	public void testGetSumOfStars()
	{		
		GameInfo gi=new GameInfo("SomeGame");
		Assert.assertEquals(0,gi.getSumOfStars());
		gi.addReview(new Review("cool",5));
		Assert.assertEquals(5,gi.getSumOfStars());

		gi.addReview(new Review("cool",5));
		Assert.assertEquals(10,gi.getSumOfStars());
		gi.addReview(new Review("ok",3));
		Assert.assertEquals(13,gi.getSumOfStars());
		gi.addReview(new Review("terrible",1));
		Assert.assertEquals(14,gi.getSumOfStars());
	}
	
	@Grade(points=15)
	@Test
	public void testAverageStarRating()
	{		
		GameInfo gi=new GameInfo("SomeGame");
		gi.addReview(new Review("cool",5));
		Assert.assertEquals(5,gi.getAverageStarRating(),0.1);

		gi.addReview(new Review("cool",5));
		Assert.assertEquals(5,gi.getAverageStarRating(),0.1);
		gi.addReview(new Review("ok",3));
		Assert.assertEquals(4.33,gi.getAverageStarRating(),0.1);
		gi.addReview(new Review("terrible",1));
		Assert.assertEquals(3.5,gi.getAverageStarRating(),0.1);
	}
	
	@Grade(points=15)
	@Test
	public void testGetListProduct()
	{		
		Assert.assertEquals(120,getListProduct(Arrays.asList(1,2,3,4,5)));
		Assert.assertEquals(0,getListProduct(Arrays.asList(1,2,0,4,5)));
		Assert.assertEquals(32,getListProduct(Arrays.asList(2,2,2,2,2)));
	}
	
	@Grade(points=20)
	@Test
	public void testGetNumberOfReviewsForGame()
	{		
		GameInfoCollection gic=new GameInfoCollection();
        
        
        gic.addGameReview("g1",new Review("cool",5));
        gic.addGameReview("g1",new Review("cool",3));
        gic.addGameReview("g2",new Review("cool",2));
        gic.addGameReview("g3",new Review("cool",2));
        
		Assert.assertEquals(2,gic.getNumberOfReviewsForGame("g1"));
		Assert.assertEquals(1,gic.getNumberOfReviewsForGame("g2"));
        gic.addGameReview("g1",new Review("cool",3));
		Assert.assertEquals(3,gic.getNumberOfReviewsForGame("g1"));
	}
	
	@Grade(points=20)
	@Test
	public void testGetAverageNumberOfStarsForGame()
	{		
		GameInfoCollection gic=new GameInfoCollection();
        
        
        gic.addGameReview("g1",new Review("cool",5));
        gic.addGameReview("g1",new Review("cool",3));
        gic.addGameReview("g2",new Review("cool",2));
        gic.addGameReview("g3",new Review("cool",2));
        
		Assert.assertEquals(4,gic.getAverageNumberOfStarsForGame("g1"), 0.1);
		Assert.assertEquals(2,gic.getAverageNumberOfStarsForGame("g2"), 0.1);
        gic.addGameReview("g1",new Review("cool",3));
        gic.addGameReview("g1",new Review("cool",3));
		Assert.assertEquals(3.5,gic.getAverageNumberOfStarsForGame("g1"), 0.1);
	}
	
}
