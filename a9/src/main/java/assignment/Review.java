package assignment;

class Review {
	public String reviewText;
	public int numberOfStars;
	
	public Review(String reviewText, int numberOfStars) {
		this.reviewText=reviewText;
		this.numberOfStars=numberOfStars;
	}
}
