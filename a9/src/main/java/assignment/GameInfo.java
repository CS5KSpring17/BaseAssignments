package assignment;

class GameInfo {
	private String title;
	// need an ArrayList (CANNOT use an array) to keep the reviews; 

	public GameInfo(String title) {
		this.title=title;
		// you may want to initialize any other variables you create here
	}
	
	public String getTitle() {
		return title;
	}
	
	// TODO - adds the review to the List of reviews. You need to keep all reviews in an array
	public void addReview(Review r) {
	}

	// TODO - returns the number of reviews which have been added to this GameInfo
	public int getNumberOfReviews() {
		return 0;
	}
	
	// TODO - returns the sum of the number of stars which have been added to this GameInfo
	// you have to calculate this from your array
	public int getSumOfStars() {
		int sum=0;
		return sum;
	}

	// TODO - returns the average number of stars for this GameInfo's reviews
	// again, have to calculate this (or at least the sum of stars) from your array
	public double getAverageStarRating() {
		return 0;
	}
}
