package assignment;


import org.junit.Assert;
import org.junit.Test;

import com.okaram.grading.Grade;



public class TestSquareOnly {

	@Test
	public void testConstructorAndGetSide()
	{		
        Square s=new Square(3);
		Assert.assertEquals(3.0,s.getSide(),0.01);
	}
	
	
	@Test
	public void testArea_3()
	{		
        Square s=new Square(3);
		Assert.assertEquals(9.0,s.getArea(),0.01);
	}	

	@Test
	public void testArea_4()
	{		
		Square s=new Square(4);
		Assert.assertEquals(16.0,s.getArea(),0.01);
	}	

	@Test
	public void testPerimeter_3()
	{		
        Square s=new Square(3);
		Assert.assertEquals(12.0,s.getPerimeter(),0.01);
	}	

	@Test
	public void testPerimeter_4()
	{		
        Square s=new Square(4);
		Assert.assertEquals(16.0,s.getPerimeter(),0.01);
	}	
}
