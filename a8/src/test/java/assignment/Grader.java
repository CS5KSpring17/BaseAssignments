package assignment;


import org.junit.Assert;
import org.junit.Test;

import com.okaram.grading.Grade;

import static assignment.Assignment8.*;

public class Grader {

	@Grade(points=30)
	@Test
	public void testSquare()
	{		
        Square s=new Square(3);
		Assert.assertEquals(9.0,s.getArea(),0.01);
		Assert.assertEquals(12.0,s.getPerimeter(),0.01);

		s=new Square(4);
		Assert.assertEquals(16.0,s.getArea(),0.01);
		Assert.assertEquals(16.0,s.getPerimeter(),0.01);
	}	

	@Grade(points=30)
	@Test
	public void testRegularPolygon()
	{		
        RegularPolygon s=new RegularPolygon(3,4.0,5.0);
		Assert.assertEquals(30.0,s.getArea(),0.01);
		Assert.assertEquals(12.0,s.getPerimeter(),0.01);

		s=new RegularPolygon(4,4.0,5.0);
		Assert.assertEquals(40.0,s.getArea(),0.01);
		Assert.assertEquals(16.0,s.getPerimeter(),0.01);
	}	

	@Grade(points=20)
	@Test
	public void testAverageArea()
	{		
		IShape[] shapes={new Rectangle(2,3), new Circle(1)};
		Assert.assertEquals(4.57,getAverageArea(shapes),0.01);

		IShape[] shapes2={new Rectangle(2,3)};
		Assert.assertEquals(6,getAverageArea(shapes2),0.01);
	}	

	@Grade(points=20)
	@Test
	public void testAveragePerimeter()
	{		
		IShape[] shapes={new Rectangle(2,3), new Circle(1)};
		Assert.assertEquals(8.14,getAveragePerimeter(shapes),0.01);

		IShape[] shapes2={new Rectangle(2,3)};
		Assert.assertEquals(10,getAveragePerimeter(shapes2),0.01);
	}	
}
