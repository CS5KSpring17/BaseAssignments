package assignment;

import java.util.Scanner;
import java.util.Random;

/*
- IShape.java defines an interface. 
- Rectangle.java and Circle.java are provided to you as examples of classes that implement that interface.
- TODO - You need to complete Square.java
- TODO - You need to define a class, RegularPolygon, from scratch, that implements that interface
*/

// TODO - define a class Square and implement functions. The constructor takes one double only (the side of the square).
// I am giving you the skeleton. You also need to implement getSide
class Square implements IShape {
	public Square(double side) {
	}
	
	double getSide(){return 0;}
	
	@Override
	public double getArea() { return 0; }
	
	@Override
	public double getPerimeter() { return 0; } 
	
	// should return something like Square[side=x] ; not tested
	@Override
	public String toString(){ return "Square"; }
	
	// this should return "square with side x. where x is the length of the side; not tested
	@Override
	public String forProblemStatement() {
        return "square.";
	}
}

// TODO - define a class RegularPolygon and implement functions. The constructor takes three parameters, the first is the number of sides (an int), the second is the size size and the third is the apothem.
// I googled for you - http://www.mathwords.com/a/area_regular_polygon.htm :)
// fun extension - make fromProblemStatement use right names for Regular polygons of sm
// no skeleton provided

class ShapeFactory {
	public static Random randomGenerator=new Random();
	public static IShape getShapeInstance(String kindOfShape, double param1, double param2, double param3) {
		kindOfShape=kindOfShape.toLowerCase();
		if(kindOfShape.equals("circle")) {
			return new Circle(param1);
		} else if (kindOfShape.equals("rectangle")) {
			return new Rectangle(param1, param2);
		}
        // TODO - after you've implemented your Square class, add it here so this function may return one
        // TODO - after you've implemented your RectangularPolygon class, add it here so this function may return one        
		else
			return null;
	}
	
	public static IShape getRandomShape() {
		String[] shapeNames={"circle", "rectangle"};
        // TODO - after you've implemented Square and RectangularPolygon, add them to the array above so they work
		int shape=randomGenerator.nextInt(shapeNames.length);
		int param1=randomGenerator.nextInt(10)+1;
		int param2=randomGenerator.nextInt(10)+1;
		int param3=randomGenerator.nextInt(10)+1;
        
        return getShapeInstance(shapeNames[shape],param1,param2,param3);
	}
}

public class Assignment8 {
	
    public static void askAreaProblem(IShape s, double acceptableVariance)
    {
        System.out.println("What is the area of a "+s.forProblemStatement());
        double answer=new Scanner(System.in).nextDouble();
        double area=s.getArea();
        if(Math.abs(answer-area)<=acceptableVariance) {
            System.out.println("Yay ! your answer, "+answer+" is correct");
        } else {
            System.out.println("Sorry :( your answer, "+answer+" is not correct.");
            System.out.println("The correct answer is "+area);
        }
    }
    
    public static void askPerimeterProblem(IShape s, double acceptableVariance)
    {
        System.out.println("What is the perimeter of a "+s.forProblemStatement());
        double answer=new Scanner(System.in).nextDouble();
        double perimeter=s.getPerimeter();
        if(Math.abs(answer-perimeter)<=acceptableVariance) {
            System.out.println("Yay ! your answer, "+answer+" is correct");
        } else {
            System.out.println("Sorry :( your answer, "+answer+" is not correct.");
            System.out.println("The correct answer is "+perimeter);
        }
    }

 
    // TODO - you need to implement this function
    public static double getAverageArea(IShape[] shapes)
    {
    	return 0;
    }
    
    // TODO - you need to implement this function
    public static double getAveragePerimeter(IShape[] shapes)
    {
    	return 0;
    }

    public static void main(String[] args) {
        IShape shp=ShapeFactory.getRandomShape();
        askAreaProblem(shp,.01);
        askPerimeterProblem(shp,.01);
    }
}
