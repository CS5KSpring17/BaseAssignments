package assignment;

// Provided to you as an example
public class Rectangle implements IShape {
	private double width, height;
	public Rectangle(double width, double height) {
		this.width=width;
		this.height=height;
	}
	
	@Override
	public double getArea() { return width*height; }
	
	@Override
	public double getPerimeter() { return 2*(width+height); } 
	
	@Override
	public String toString(){ return "Rectangle[width:"+width+" height:"+height+"]"; }
	
	@Override
	public String forProblemStatement() {
        return "rectangle with width "+ width +" and height "+height+".";
	}
}
