package assignment;

public interface IShape  {
	double getArea();
	double getPerimeter();
    String forProblemStatement(); 
}