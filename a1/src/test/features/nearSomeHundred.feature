Feature: nearSomeHundred (15 pts)
  Scenario: 1 -- 90 is near some hundred
  Then 90 is near some hundred

  Scenario: 2 -- 89 is not near some hundred
    Then 80 is not near some hundred

  Scenario: 3 -- 7 is not near some hundred
    Then 7 is not near some hundred

  Scenario: 4 -- 95 is near some hundred
    Then 95 is near some hundred

  Scenario: 5 -- 103 is near some hundred
    Then 103 is near some hundred

  Scenario: 6 -- 110 is near some hundred
    Then 110 is near some hundred

  Scenario: 7 -- 111 is not near some hundred
    Then 111 is not near some hundred

  Scenario: 8 -- 231 is not near some hundred
    Then 231 is not near some hundred

  Scenario: 9 -- -5 is not near some hundred
    Then -5 is not near some hundred

  Scenario: 10 -- -100 is not near some hundred
    Then -100 is not near some hundred

  Scenario: 11 -- 195 is near some hundred
    Then 195 is near some hundred

  Scenario: 12 -- 203 is near some hundred
    Then 203 is near some hundred

  Scenario: 13 -- 210 is near some hundred
    Then 210 is near some hundred

  Scenario: 14 -- 211 is not near some hundred
    Then 211 is not near some hundred
