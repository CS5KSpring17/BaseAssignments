Feature: triangle area (15 pts)
  Scenario: 1
  When the base is 3.5
  And the height is 4.4
  Then the are of the triangle is 7.7

  Scenario: 2
    When the base is 1
    And the height is 2
    Then the are of the triangle is 1

  Scenario: 3
    When the base is 1
    And the height is 1
    Then the are of the triangle is 0.5

  Scenario: 4
    When the base is 3
    And the height is 4
    Then the are of the triangle is 6

