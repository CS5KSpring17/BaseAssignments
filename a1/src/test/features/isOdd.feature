Feature: isOdd (15 pts)
  Scenario: 1 -- 7 is odd
  Then 7 is odd

  Scenario: 2 -- 8 is not odd
    Then 8 is not odd

  Scenario: 3 -- 127 is odd
    Then 127 is odd

  Scenario: 4 -- 120 is not odd
    Then 120 is not odd

  Scenario: 5 -- 75 is odd
    Then 75 is odd

  Scenario: 6 -- 724 is odd
    Then 724 is not odd

