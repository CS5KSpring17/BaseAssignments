Feature: nearHundred (15 pts)
  Scenario: 1 -- 90 is near hundred
  Then 90 is near hundred

  Scenario: 2 -- 89 is not near hundred
    Then 89 is not near hundred

  Scenario: 3 -- 7 is not near hundred
    Then 7 is not near hundred

  Scenario: 4 -- 95 is near hundred
    Then 95 is near hundred

  Scenario: 5 -- 103 is near hundred
    Then 103 is near hundred

  Scenario: 6 -- 110 is near hundred
    Then 110 is near hundred

  Scenario: 7 -- 111 is not near hundred
    Then 111 is not near hundred

  Scenario: 8 -- 231 is not near hundred
    Then 231 is not near hundred

  Scenario: 9 -- -5 is not near hundred
    Then -5 is not near hundred

  Scenario: 10 -- -100 is not near hundred
    Then -100 is not near hundred

