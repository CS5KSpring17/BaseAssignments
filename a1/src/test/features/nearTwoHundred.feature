Feature: nearTwoHundred (15 pts)
  Scenario: 1 -- 190 is near two hundred
  Then 190 is near two hundred

  Scenario: 2 -- 189 is not near two hundred
    Then 189 is not near two hundred

  Scenario: 3 -- 7 is not near two hundred
    Then 7 is not near two hundred

  Scenario: 4 -- 195 is near two hundred
    Then 195 is near two hundred

  Scenario: 5 -- 203 is near two hundred
    Then 203 is near two hundred

  Scenario: 6 -- 210 is near two hundred
    Then 210 is near two hundred

  Scenario: 7 -- 211 is not near two hundred
    Then 211 is not near two hundred

  Scenario: 8 -- 231 is not near two hundred
    Then 231 is not near two hundred

  Scenario: 9 -- -5 is not near two hundred
    Then -5 is not near two hundred

  Scenario: 10 -- -100 is not near two hundred
    Then -100 is not near two hundred

