Feature: sleepIn (15 pts)
  Scenario: weekday, vacation
  When it is a weekday
  And we are on vacation
  Then we can sleep in

  Scenario: weekday, no vacation
    When it is a weekday
    And we are not on vacation
    Then we can not sleep in

  Scenario: not weekday, vacation
    When it is not a weekday
    And we are on vacation
    Then we can sleep in

  Scenario: not weekday, no vacation
    When it is not a weekday
    And we are not on vacation
    Then we can sleep in


