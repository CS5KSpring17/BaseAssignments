package assignment;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.io.*;
import java.util.Scanner;
import static assignment.Assignment1.*;

public class CucumberSteps {
    double base, height;

    boolean isWeekday, areWeOnVacation;
    static boolean is(String maybe) {
        return !maybe.contains("not");
    }

    @Then("^(\\d+) (is|is not) odd$")
    public void is_odd(int n, String maybe) throws Throwable {
        Assert.assertEquals(is(maybe), isOdd(n));
    }

    @When("^it (is|is not) a weekday$")
    public void it_is_a_weekday(String maybe) throws Throwable {
        isWeekday=is(maybe);
    }

    @When("^we (are|are not) on vacation$")
    public void we_are_on_vacation(String maybe) throws Throwable {
        areWeOnVacation=is(maybe);
    }

    @Then("^we (can|can not) sleep in$")
    public void we_can_sleep_in(String maybe) throws Throwable {
        Assert.assertEquals(is(maybe), sleepIn(isWeekday,areWeOnVacation));
    }

    @Then("^(-?\\d+) (is|is not) near hundred$")
    public void is_near_hundred(int n,String maybe) throws Throwable {
        Assert.assertEquals(is(maybe),nearHundred(n));
    }

    @Then("^(-?\\d+) (is|is not) near two hundred$")
    public void is_near_two_hundred(int n,String maybe) throws Throwable {
        Assert.assertEquals(is(maybe),nearTwoHundred(n));
    }

    @Then("^(-?\\d+) (is|is not) near some hundred$")
    public void is_near_some_hundred(int n,String maybe) throws Throwable {
        Assert.assertEquals(is(maybe),nearSomeHundred(n));
    }

    @When("^the base is (\\d+.?\\d*)$")
    public void the_base_is(double base) throws Throwable {
        this.base=base;
    }

    @When("^the height is (\\d+.?\\d*)$")
    public void the_height_is(double height) throws Throwable {
        this.height=height;
    }

    @Then("^the are of the triangle is (\\d+.?\\d*)$")
    public void the_are_of_the_triangle_is(double area) throws Throwable {
        Assert.assertEquals(area, triangleArea(base,height), 0.1);
    }

    @Then("^There is (\\d+) feet in (\\d+) yards")
    public void there_is_yards_in_feet(int feet, int yards) throws Throwable {
        Assert.assertEquals(feet, yardsToFeet(yards));
    }

}
